import { Component } from '@angular/core';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent {
 constructor(){

 }
 ngOnInit():void {

 }
 course= [
  {'id':1, 'name':'Learn Angular', 'description':'Angular est une plateforme de développement, construite sur TypeScript. En tant que plateforme, Angular comprend : Un cadre basé sur des composants pour la ..','image':'assets/img/Angularim.png'},
  {'id':2, 'name':'Learn TypeScript', 'description':'Angular est une plateforme de développement, construite sur TypeScript.  Angular comprend : Un cadre basé sur des composants pour la ..','image':'assets/img/title.avif'},

  {'id':3, 'name':'Learn Node js', 'description':'Angular est une plateforme de développement, construite sur TypeScript. En tant que plateforme, Angular comprend : Un cadre basé sur des composants pour la ..','image':'assets/img/node.png'},

  {'id':4, 'name':'Learn java', 'description':'Angular est une plateforme de développement, construite sur TypeScript. En tant que plateforme, Angular comprend : Un cadre basé sur des composants pour la ..','image':'assets/img/javaa.png'}

 ]
}
