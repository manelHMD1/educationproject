import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CourseComponent } from './components/course/course.component';
import { JoinnowComponent } from './components/joinnow/joinnow.component';
import { AboutComponent } from './components/about/about.component';

const routes: Routes = [
 {path:'',component:HomeComponent},
 {path:'course',component:CourseComponent},
 {path:'join',component:JoinnowComponent},
 {path:'about',component:AboutComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
